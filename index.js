const AWS = require('aws-sdk')
const dynamodb = new AWS.DynamoDB({apiVersion: '2012-08-10'})

exports.handler = (event, context, callback) => {
    let playlist = event.body.playlist
    let tracks = event.body.tracks

    let putData = (playlistId, trackData) => {
        const paramsToRead = {
            Key: {
                "playlistId": {
                    S: playlistId
                }
            }, 
            TableName: "playlistItems"
        }

        const finalData = trackData.map(item => {
            if (item.title === 'Private video' || item.title === 'Deleted video') {
                dynamodb.getItem(paramsToRead, (err, data) => {
                    if (err) {
                        console.log('Error. Getting item(s) from dynamodb failed: ', JSON.stringify(err, null, 2))
                        callback(err, null)
                    }
                    else {
                        try {
                            if (data.Item) {
                                return JSON.parse(data.Item.tracks.S)                                
                            }
                        } catch(e) {
                            console.log('Error. Getting item(s) from dynamodb failed: ', JSON.stringify(err, null, 2))
                            callback(err, null)
                        }
                    }
                })
            }

            return item
        })
        
        const paramsToWrite = {
            TableName: 'playlistItems',
            Item: {
                'playlistId' : {S: playlistId},
                'tracks' : {S: JSON.stringify(finalData)}
            }
        }

        dynamodb.putItem(paramsToWrite, (err, data) => {
            if (err) {
                console.log('Error putting item into dynamodb failed: ', JSON.stringify(err, null, 2))
                callback(e, null)
            }
            else {
                console.log('great success: ' + JSON.stringify(data, null, 2))
                callback(null, 'success')
            }
        })
    }

    putData(playlist, tracks)
}